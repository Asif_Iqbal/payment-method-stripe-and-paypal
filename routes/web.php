<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/dashboard', 'AdminController@dashboard');
Route::get('/admin_dashboard', 'AdminController@admin_dashboard')->name('admin.dashboard');
Route::get('/customer', 'AdminController@customer')->name('customer.show');
Route::get('/add_customer', 'AdminController@add_customer')->name('add.customer');
Route::post('/post_customer', 'AdminController@post_customer')->name('post.customer');
Route::get('/product', 'AdminController@product')->name('product.show');
Route::get('/add_product', 'AdminController@add_product')->name('add.product');
Route::post('/post_product', 'AdminController@post_product')->name('post.product');
Route::get('/invoice', 'AdminController@invoice')->name('invoice.show');
Route::get('/add_invoice', 'AdminController@add_invoice')->name('add.invoice');
Route::post('/post_invoice', 'AdminController@post_invoice')->name('post.invoice');
Route::get('/invoice_view/{id}', 'AdminController@invoice_view')->name('view.invoice');
Route::get('/stripe/{id}', 'AdminController@stripe')->name('stripe');
Route::post('/payment_stripe/{id}', 'AdminController@payment_stripe')->name('payment.stripe');
Route::get('/paypal/{id}', 'PaymentController@paypal')->name('paypal');
Route::post('paypal', 'PaymentController@payWithpaypal');
Route::get('status', 'PaymentController@getPaymentStatus');
