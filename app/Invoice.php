<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['customer', 'phone', 'currancy', 'date', 'description', 'quantity', 'price', 'discount'];

    public function customers()
    {
        return $this->hasOne(Customer::class, 'id');
    }
}
