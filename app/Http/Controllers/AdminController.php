<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Invoice;
use App\Mail\SendEmail;
use App\Product;
use Illuminate\Http\Request;
use Session;
use Mail;



class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function admin_dashboard()
    {
        return view('admin.admin_dashboard');
    }
    public function customer()
    {
        $customers = Customer::all();
        return view('admin.customer.index', compact('customers'));
    }
    public function add_customer()
    {
        return view('admin.customer.post');
    }

    public function post_customer(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
        ]);
        $customers = new Customer;
        $customers->name = $request->name;
        $customers->address = $request->address;
        $customers->email = $request->email;
        $customers->save();
        Session::flash('success', 'Customer Created');
        return redirect()->back();
    }
    public function product()
    {
        $products = Product::all();
        return view('admin.product.index', compact('products'));
    }

    public function add_product()
    {
        return view('admin.product.post');
    }
    public function post_product(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'category' => 'required',
            'description' => 'required',
        ]);
        $products = new Product;
        $products->name = $request->name;
        $products->category = $request->category;
        $products->description = $request->description;
        $products->save();
        Session::flash('success', 'Product Created');
        return redirect()->back();
    }
    public function add_invoice()
    {
        $customers = Customer::all();
        $products = Product::all();
        return view('admin.invoice.post', compact('customers', 'products'));
    }
    public function post_invoice(Request $request)
    {
        $validatedData = $request->validate([
            'customer' => 'required',
            'email' => 'required',
            'description' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'currancy' => 'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);
        $invoices = new Invoice;
        $invoices->customer = $request->customer;
        $invoices->email = $request->email;
        $invoices->phone = $request->phone;
        $invoices->currancy = $request->currancy;
        $invoices->date = $request->date;
        $invoices->description = $request->description;
        $invoices->quantity = $request->quantity;
        $invoices->price = $request->price;
        $invoices->discount = $request->discount;
        $invoices->save();
        Session::flash('success', 'Invoice Created');
        return redirect()->back();
    }
    public function invoice()
    {
        $invoices = Invoice::all();
        return view('admin.invoice.index', compact('invoices'));
    }
    public function invoice_view($id)
    {
        $invoices = Invoice::find($id);
        return view('admin.invoice.invoice', compact('invoices'));
    }

    public function stripe($id)
    {
        $invoices = Invoice::find($id);
        return view('admin.stripe.index', compact('invoices'));
    }



    public function payment_stripe(Request $request, $id)
    {
        $invoices = Invoice::find($id);
        Mail::to('asifiqbal1537@gmail.com')->send(new SendEmail($invoices));


        \Stripe\Stripe::setApiKey('sk_test_9XFRfzHKKgTQJVy2pKaGBY5j00r6hYP1Un');
        try {

            \Stripe\Charge::create(array(
                "amount" => 200 * 100,
                "currency" => "usd",
                "source" => $request->input('stripeToken'),
                "description" => "Test payment."
            ));



            Session::flash('success-message', 'Payment done successfully ! and send the confirmation message');
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('fail-message', "Error! Please Try again.");
            return redirect()->back();
        }
    }
}
