<style>
.info{
    padding-left:20px;

}
.table{
    border-collapse: initial;
}
.total{
    border-left: 4px solid #928787ad;
}
.payment{
    padding: 20px;
    padding-left: 309px;
    font-family: cursive;
    color: var(--success);
    font-size: 50px;
    font-weight: 600;
}
</style>
@extends('layouts.app')
@section('content')
<div class="container">
    <div class="invoice" style="padding-left:650px; ">  <h2>Invoice</h2>
    </div>

    <div class="row" style="padding-left:150px;margin-top:20px;">

        {{-- @foreach ($invoices as $invoice) --}}
    <div class=" col-sm-6">
        @if ($invoices->customers)
        <p>{{ $invoices->customers->address }}</p>
        @else
       <p></p>
        @endif
        <p>phone:<span class="info">{{ $invoices->phone }}</span></p>
        @if ($invoices->customers)
        <p>Email:<span class="info">{{ $invoices->customers->email }}</span></p>
        @else
       <p></p>
        @endif

    </div>
    <div class=" col-sm-6 info">
            <p>Date:<span class="info" style="padding-left:74px;">{{ $invoices->date }}</span></p>
            <p>Trancation Id:<span class="info"></span>{{ $invoices->id }}</span></p>
        </div>


        <table class="table" style="margin-top:20px;border:4px solid #928787ad;">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Item Description</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Unit</th>
                    <th scope="col">Price</th>
                    <th scope="col">Discount</th>
                    <th class="total" scope="col">Total</th>
                  </tr>
                </thead>
                <tr>
                        <th scope="row">{{ $invoices->id }}</th>
                        <td>{{ $invoices->description }}</td>
                        <td>{{ $invoices->quantity }}</td>
                        <td> each </td>
                        @if ($sum=$invoices->quantity*$invoices->price)
                        <td>{{ $sum}}</td>
                        @endif
                        <td>{{ $invoices->discount }}</td>
                        <td class="total">{{ $sum }}</td>

                </tr>
                <div class="info">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="padding-top:100px;">Sub-total</td>
                        <td style="padding-top:100px;"></td>
                        <td></td>
                        <td class="total" style="padding-top:100px;"> {{ $sum }} </td>
                </div>
                <tr>
                        <div class="info">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Shipping Cost</td>
                                <td></td>
                                <td></td>
                                <td class="total">45</td>
                        </div>
                </tr>
                <tr>
                        <div class="info">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total Order</td>
                                <td></td>
                                <td></td>
                                <td class="total">{{ $sum+45 }}</td>
                        </div>
                </tr>
        </table>
        <div>
            <h1 class="payment">Payment</h1>

                <a href="{{ route('paypal',$invoices->id) }}"><img style="border-radius:50%;" src="{{ url('paypal1.png') }}" class="img-responsive" height="100px;" width="150px"></a>

                <a href="{{ route('stripe',$invoices->id) }}"><img style="border-radius:50%;" src="{{ url('stripe.png') }}" class="img-responsive" height="100px;" width="150px"></a>

        </div>

</div>



</div>

@stop
