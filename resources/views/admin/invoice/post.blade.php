@extends('admin.dashboard')
@section('content1')
<div class="main-panel">
    <div class="content">
            <form class="form-horizontal" action="{{ route('post.invoice')}}" method="post" enctype="multipart/form-data">
            @csrf

            @if ($message = Session::get('success'))
                <div class="w3-panel w3-green w3-display-container">
                    <span onclick="this.parentElement.style.display='none'"
                            class="w3-button w3-green w3-large w3-display-topright">&times;</span>
                    <p>{!! $message !!}</p>
                </div>
                <?php Session::forget('success');?>
                @endif

            <div class="form-group">
                    <label for="customer">Customer</label>

                    <select class="form-control" id="$customer" name="customer">
                            @foreach ($customers as $customer)
                      <option>{{ $customer->name }}</option>
                      @endforeach
                    </select>

                  </div>
                  <div class="form-group">
                        <label for="phone">Email</label>
                        <input type="email" name="email" class="form-control"  placeholder="Email">
                      </div>
                  <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="number" name="phone" class="form-control"  placeholder="Phone">
                      </div>
                  <div class="form-group">
                        <label for="exampleFormControlSelect1">Currancy</label>
                        <select class="form-control" id="currancy" name="currancy">
                          <option value="Euros">Euros</option>
                          <option value="Dollar">Dollar</option>
                          <option value="Taka">Taka</option>
                        </select>
                      </div>

                      <div class="form-group">
                            <label for="example-date-input" class="col-2 col-form-label">Date</label>
                            <div class="form-control">
                              <input class="form-control" type="date" value="2011-08-19" id="date" name="date">
                            </div>
                          </div>


                          <div class="form-group">
                                <label for="description">Description</label>

                                <select class="form-control" id="$customer" name="description">
                                        @foreach ($products as $product)
                                  <option>{{ $product->description }}</option>
                                  @endforeach
                                </select>

                              </div>


            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="number" name="quantity" class="form-control"  placeholder="Quantity">
              </div>
              <div class="form-group">
                    <label for="price">Unit Price</label>
                    <input type="number" name="price" class="form-control"  placeholder="Unit Price">
                  </div>




            <button type="submit" class="btn btn-primary">Create Invoice</button>
          </form>

    </div>
</div>
@stop
