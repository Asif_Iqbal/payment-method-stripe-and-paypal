@extends('admin.dashboard')
@section('content1')
<div class="main-panel">
    <div class="content">

        <table class="table table-info" style="margin-top:20px;">
                @if ($message = Session::get('success'))
                <div class="w3-panel w3-green w3-display-container">
                    <span onclick="this.parentElement.style.display='none'"
                            class="w3-button w3-green w3-large w3-display-topright">&times;</span>
                    <p>{!! $message !!}</p>
                </div>
                <?php Session::forget('success');?>
                @endif
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">customer</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">currancy</th>
                    <th scope="col">date</th>
                    <th scope="col">description</th>
                    <th scope="col">quantity</th>
                    <th scope="col">price</th>
                    <th scope="col">discount</th>
                    <th scope="col">Created_at</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($invoices as $key=>$invoice)


                  <tr>
                    <th scope="row">{{ $invoice->id }}</th>
                    <td>{{ $invoice->customer }}</td>
                    <td>{{ $invoice->email }}</td>
                    <td>{{ $invoice->phone }}</td>
                    <td>{{ $invoice->currancy }}</td>
                    <td>{{ $invoice->date }}</td>
                    <td>{{ $invoice->description }}</td>
                    <td>{{ $invoice->quantity }}</td>
                    <td>{{ $invoice->price }}</td>
                    <td>{{ $invoice->description }}</td>
                    <td>{{ $invoice->created_at }}</td>
                    <td><a href="{{ route('view.invoice',$invoice->id) }}" class="btn btn-info">View Invoice</a></td>
                  </tr>
                  @endforeach

                </tbody>
              </table>

    </div>
</div>
@stop
