@extends('admin.dashboard')
@section('content1')
<div class="main-panel">
    <div class="content">
            <form class="form-horizontal" action="{{ route('post.product')}}" method="post" enctype="multipart/form-data">
            @csrf
            @if ($message = Session::get('success'))
                <div class="w3-panel w3-green w3-display-container">
                    <span onclick="this.parentElement.style.display='none'"
                            class="w3-button w3-green w3-large w3-display-topright">&times;</span>
                    <p>{!! $message !!}</p>
                </div>
                <?php Session::forget('success');?>
                @endif
            <div class="form-group">
              <label for="name">Product name</label>
              <input type="text" name="name" class="form-control"  placeholder="Name">
            </div>
            <div class="form-group">
                <label for="category">Category</label>
                <input type="text" name="category" class="form-control"  placeholder="Category">
              </div>
              <div class="form-group">
                <label for="description">Description</label>
                <textarea type="text" name="description" class="form-control"  placeholder="Description" rows="5"></textarea>

              </div>

            <button type="submit" class="btn btn-primary">Add Product</button>
          </form>

    </div>
</div>
@stop
