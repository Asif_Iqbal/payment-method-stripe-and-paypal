@extends('admin.dashboard')
@section('content1')
<div class="main-panel">
    <div class="content">

        <a class="btn btn-info" href="{{ route('add.product') }}">Add Product</a>

        <table class="table table-info" style="margin-top:20px;">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Description</th>
                    <th scope="col">Created_at</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($products as $key=>$product)


                  <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->category }}</td>
                    <td>{{ $product->description }}</td>
                    <td>{{ $product->created_at }}</td>
                  </tr>
                  @endforeach

                </tbody>
              </table>

    </div>
</div>
@stop
