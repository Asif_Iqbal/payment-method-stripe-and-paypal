<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title> Dashboard</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<link rel="stylesheet" href="{{ 'admin/css/bootstrap.min.css' }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="{{ 'admin/css/ready.css' }}">
	<link rel="stylesheet" href="{{ 'admin/css/demo.css' }}">
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<div class="logo-header">
				<a href="{{ route('admin.dashboard') }}" class="logo">
					 Dashboard
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<button class="topbar-toggler more"><i class="la la-ellipsis-v"></i></button>
			</div>
			<nav class="navbar navbar-header navbar-expand-lg">
				<div class="container-fluid">

					<form class="navbar-left navbar-form nav-search mr-md-3" action="">
						<div class="input-group">
							<input type="text" placeholder="Search ..." class="form-control">
							<div class="input-group-append">
								<span class="input-group-text">
									<i class="la la-search search-icon"></i>
								</span>
							</div>
						</div>
                    </form>

					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">

						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="la la-bell"></i>
								<span class="notification">3</span>
							</a>
							<ul class="dropdown-menu notif-box" aria-labelledby="navbarDropdown">
								<li>
									<div class="dropdown-title">You have 4 new notification</div>
								</li>
								<li>
									<div class="notif-center">
										<a href="#">
											<div class="notif-icon notif-primary"> <i class="la la-user-plus"></i> </div>
											<div class="notif-content">
												<span class="block">
													New user registered
												</span>
												<span class="time">5 minutes ago</span>
											</div>
										</a>
										<a href="#">
											<div class="notif-icon notif-success"> <i class="la la-comment"></i> </div>
											<div class="notif-content">
												<span class="block">
													Rahmad commented on Admin
												</span>
												<span class="time">12 minutes ago</span>
											</div>
										</a>
										<a href="#">

											<div class="notif-content">
												<span class="block">
													Reza send messages to you
												</span>
												<span class="time">12 minutes ago</span>
											</div>
										</a>
										<a href="#">
											<div class="notif-icon notif-danger"> <i class="la la-heart"></i> </div>
											<div class="notif-content">
												<span class="block">
													Farrah liked Admin
												</span>
												<span class="time">17 minutes ago</span>
											</div>
										</a>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="la la-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown">


								<!-- /.dropdown-user -->
							</li>
						</ul>
					</div>
                </nav>

            </div>


			<div class="sidebar">

				<div class="scrollbar-inner sidebar-wrapper">
					<div class="user">

						<div class="info">
							<a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="true">

								<span>
                                        @auth
									{{ Auth::user()->name }}
                                    <span class="user-level">Administrator</span>
                                    @endauth
                                    <span class="caret"></span>
                                    @guest
                                    User

                                    @endguest
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample" aria-expanded="true" style="">
								<ul class="nav">
									<li>
										<a href="#profile">
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                              document.getElementById('logout-form').submit();">
                                                 {{ __('Logout') }}
                                             </a>

                                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                 @csrf
                                             </form>
										</a>
									</li>
								</ul>
							</div>
						</div>
                    </div>


					<ul class="nav">

						<li class="nav-item">
							<a href="{{ route('customer.show') }}">
								<i class="la la-table"></i>
								<p>Create Customer</p>

							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('product.show') }}">
								<i class="la la-keyboard-o"></i>
								<p>Create Product</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('add.invoice') }}">
								<i class="la la-th"></i>
								<p>Create Invoice</p>

							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('invoice.show') }}">
								<i class="la la-bell"></i>
								<p>View Invoice</p>

							</a>
						</li>



					</ul>
				</div>
            </div>

            @yield('content1')




        </div>

    </div>



</body>
<script src="{{ "admin/js/core/jquery.3.2.1.min.js" }}"></script>
<script src="{{ "admin/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js" }}"></script>
<script src="{{ "admin/js/core/popper.min.js" }}"></script>
<script src="{{ "admin/js/core/bootstrap.min.js" }}"></script>
<script src="{{ 'admin/js/plugin/chartist/chartist.min.js' }}"></script>
<script src="{{ 'admin/js/plugin/chartist/plugin/chartist-plugin-tooltip.min.js' }}"></script>
<script src="{{ 'admin/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js' }}"></script>
<script src="{{ 'admin/js/plugin/jquery-mapael/jquery.mapael.min.js' }}"></script>
<script src="{{ 'admin/js/plugin/jquery-mapael/maps/world_countries.min.js' }}"></script>
<script src="{{ 'admin/js/plugin/chart-circle/circles.min.js' }}"></script>
<script src="{{ 'admin/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js' }}"></script>
<script src="{{ 'admin/js/ready.min.js' }}"></script>
<script src="{{ 'admin/js/demo.js' }}"></script>

</html>
