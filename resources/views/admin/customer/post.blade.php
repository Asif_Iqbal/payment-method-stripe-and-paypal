@extends('admin.dashboard')
@section('content1')
<div class="main-panel">
    <div class="content">
            <form class="form-horizontal" action="{{ route('post.customer')}}" method="post" enctype="multipart/form-data">
            @csrf
            @if ($message = Session::get('success'))
                <div class="w3-panel w3-green w3-display-container">
                    <span onclick="this.parentElement.style.display='none'"
                            class="w3-button w3-green w3-large w3-display-topright">&times;</span>
                    <p>{!! $message !!}</p>
                </div>
                <?php Session::forget('success');?>
                @endif
            <div class="form-group">
              <label for="name">Customer Name</label>
              <input type="text" name="name" class="form-control"  placeholder="Name">
            </div>
            <div class="form-group">
                <label for="name">Customer Address</label>
                <input type="text" name="address" class="form-control"  placeholder="Address">
              </div>
              <div class="form-group">
                <label for="email">Customer Email</label>
                <input type="text" name="email" class="form-control"  placeholder="Email">
              </div>

            <button type="submit" class="btn btn-primary">Add Customer</button>
          </form>

    </div>
</div>
@stop
