@extends('admin.dashboard')
@section('content1')
<div class="main-panel">
    <div class="content">

        <a class="btn btn-info" href="{{ route('add.customer') }}">Add Customer</a>

        <table class="table table-info"  style="margin-top:20px;">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created_at</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $key=>$customer)


                  <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $customer->name }}</td>
                    <td>{{ $customer->address }}</td>
                    <td>{{ $customer->email }}</td>
                    <td>{{ $customer->created_at }}</td>
                  </tr>
                  @endforeach

                </tbody>
              </table>

    </div>
</div>
@stop
