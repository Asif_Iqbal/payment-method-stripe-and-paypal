@extends('layouts.app')
@section('content')

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>
    <div class="w3-container">
        @if ($message = Session::get('success'))
        <div class="w3-panel w3-green w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
    				class="w3-button w3-green w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('success');?>
        @endif

        @if ($message = Session::get('error'))
        <div class="w3-panel w3-red w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
    				class="w3-button w3-red w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('error');?>
        @endif

    	<form class="w3-container w3-display-middle w3-card-4 w3-padding-16" style="width:40%;" method="POST" id="payment-form"
          action="{{ url('paypal') }} ">
    	  <div class="w3-container w3-teal w3-padding-16" style="padding-left: 196px;
          font-family: cursive;
          font-size: 28px;">Paywith Paypal</div>
    	  {{ csrf_field() }}
    	  <h2 class="w3-text pay"style="color: var(--teal);
          font-size: 20px;
          padding-bottom: 45px;
          font-family: cursive;">Payment Form</h2>

          <label class="w3-text-blue"><b>Total Amount</b></label>
          @if ($sum=$invoices->quantity*$invoices->price)
          <input value="{{ $sum+45 }}" class="w3-input w3-border" id="amount" type="none" name="amount"placeholder="{{ $sum+45 }}"></p>
          @else
          <p></p>
          @endif

    	  <button class="btn btn-info">Payment</button>
    	</form>
    </div>
</body>
</html>
@stop
